<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@include file="flyHeader.jsp"%>
<section id="main-content">

	<table class="tg">

		<tr>
			<th></th>
			<th>Nickname</th>
			<th>Acct Number</th>
			<th>Balance</th>
		</tr>

		<c:forEach var="account" items="${accounts}">
			<tr>

				<td><a href="accountDetail?accountId=${account.id}"> View </a></td>
				<td class="grey">${account.name}</td>
				<td>${account.accountNumber}</td>
				<td class="grey">${account.balance}</td>

			</tr>
		</c:forEach>
	</table>

</section>
<%@include file="flyFooter.jsp"%>