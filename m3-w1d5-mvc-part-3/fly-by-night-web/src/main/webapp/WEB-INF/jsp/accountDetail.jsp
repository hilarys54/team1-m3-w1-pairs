<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@include file="flyHeader.jsp"%>
<section id="main-content">

	<h1 style="color: #009900">${account.name}</h1>
	<p>
		<font style="font-weight: bold; font-size: 0.83em;">Balance: </font>
		${account.balance}
	</p>
	<p>
		<font style="font-weight: bold; font-size: 0.83em;">Account
			Num: </font> ${account.accountNumber}
	</p>
	<p>
		<font style="font-weight: bold; font-size: 0.83em;">Account
			Type: </font> ${account.accountType}
	</p>

	<h2>Transaction History</h2>


	<table class="tg">

		<tr>
			<th>Date</th>
			<th>Amount</th>
			<th>Description</th>
		</tr>

		<c:forEach var="account" items="${account.transactionHistory}">
			<tr>
				<td>${account.transactionDate}</td>
 				<td>${account.amount}</td>
				<td>${account.description}</td>

			</tr>
		</c:forEach>
	</table>

<a href="accountList">Back to List</a>
</section>
<%@include file="flyFooter.jsp"%>