<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@include file="movieHeader.jsp"%>

<%@page import="java.time.LocalDateTime"%>
<%@page import="java.time.format.DateTimeFormatter"%>

<h1>${movie.name}(${movie.yearReleased})</h1>

<p>
	<font style="font-weight: bold; font-size: 0.83em;">Runtime: </font>
	${movie.runningTime}
</p>


<p>${movie.description}</p>

<ul>
<h3>Cast</h3>
<c:forEach var="actor" items="${movie.cast}">
		<li>${actor}</li>	
	
	</c:forEach>

</ul>

</br>

	<% 
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yy hh:mm a");
		request.setAttribute("formatter", formatter);
  	%>


<h3>Show Times</h3>

<table class="tg">

		<tr>
			<th>Start Time</th>
			<th>Theatre</th>
		</tr>

		<c:forEach var="showTime" items="${movie.getShowTimes()}">
			<tr>
				<td>${formatter.format(showTime.getStartTime())}</td>
				<td>${showTime.getTheatre()}</td>
			</tr>
		</c:forEach>
	</table>


<%@include file="movieFooter.jsp"%>