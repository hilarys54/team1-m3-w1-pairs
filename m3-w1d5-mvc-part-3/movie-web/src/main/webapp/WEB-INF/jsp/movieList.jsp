 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 
 <%@include file="movieHeader.jsp" %>

    <h2>Local Movie Listing</h2>
    
    <table class="tg">

		<tr>
			<th></th>
			<th>Movie</th>
			<th>Genre</th>
			<th>Runtime (min)</th>
		</tr>

		<c:forEach var="movie" items="${movies}">
			<tr>

				<td><a href="movieDetail?movieId=${movie.id}"> View Times </a></td>
				<td>${movie.name} (${movie.yearReleased})</td>
				<td>${movie.genre}</td>
				<td>${movie.runningTime}</td>

			</tr>
		</c:forEach>
	</table>
    

    <%@include file="movieFooter.jsp" %>