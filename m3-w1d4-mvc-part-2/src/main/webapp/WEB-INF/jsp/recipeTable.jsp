<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="recipeHeader.jsp" %>

<h1>Recipes</h1>


		<table class="tg">
			<tr>
				<td></td>
				<c:forEach var="recipe" items="${recipes}">
					<td>
						<a href="http://localhost:8080/m3-java-mvc-views-part2-exercises-pair/recipeDetails?recipeId=${recipe.recipeId}">
							<img class="table-image" src="img/${recipe.recipeId}.jpg" />
						</a>
					</td>
				</c:forEach>

			</tr>
			<tr>
				<td>Name</td>
				<c:forEach var="recipe" items="${recipes}">
					<td class="grey">
						<a href="http://localhost:8080/m3-java-mvc-views-part2-exercises-pair/recipeDetails?recipeId=${recipe.recipeId}">
							${recipe.name}
						</a>
					 </td>
				</c:forEach>
			</tr>
			<tr>
				<td>Type</td>
				<c:forEach var="recipe" items="${recipes}">
					<td>${recipe.recipeType}</td>
				</c:forEach>
			</tr>
			<tr >
				<td>Cook Time</td>
				<c:forEach var="recipe" items="${recipes}">
					<td class="grey">${recipe.cookTimeInMinutes} min</td>
				</c:forEach>
			</tr>

			<tr>
				<td>Ingredients</td>
				<c:forEach var="recipe" items="${recipes}">
					<td>${recipe.ingredients.size()} ingredients</td>
				</c:forEach>
			</tr>
			<tr>
				<td>Rating</td>
				<c:forEach var="recipe" items="${recipes}">
					<td class="grey">
						<c:set var="starRound" value='${Math.round(recipe.averageRating)}'/>
						<img class="star-table" src="img/stars/${starRound}.png" />
					</td>

				</c:forEach>
			</tr>
		</table>
		
<%@include file="recipeFooter.jsp" %>
	