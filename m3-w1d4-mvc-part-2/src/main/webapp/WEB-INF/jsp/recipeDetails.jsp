<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="recipeHeader.jsp"%>

<div class="section1">
	<div class="image-container">
		<img class="detail-image" src="img/${recipe.recipeId}.jpg" />
	</div>
	<div class="details-container">
		<h4>${recipe.name}</h4>
		${recipe.recipeType}

			<p><font style="font-weight: bold; font-size: 0.83em;"> Cook Time:  </font> ${recipe.cookTimeInMinutes} minutes</p> 
		

		<p>${recipe.description}</p>

	</div>
</div>
<div class="section2">

	<h5>Ingredients</h5>
	<ul>
		<c:forEach var="ingredients" items="${recipe.getIngredients()}">

			<li>${ingredients.getQuantity()} ${ingredients.getName()}</li>

		</c:forEach>
	</ul>


</div>
<hr width="98%"/>
<div class="section3">
	<h5 >Preparation</h5>
	<ol>
		<c:forEach var="prep" items="${recipe.getPreparationSteps()}">

			<li>${prep}</li>

		</c:forEach>
	</ol>

</div>


<!-- use a page name and pass the title into the page URL-->

























<%@include file="recipeFooter.jsp"%>