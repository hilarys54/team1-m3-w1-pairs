<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="recipeHeader.jsp" %>

		<h1>Recipes</h1>
		
		<c:set var="starRound" value="1" />
	<div class="container">
		
		
		
		<c:forEach var="recipe" items="${recipes}">
			<a href="http://localhost:8080/m3-java-mvc-views-part2-exercises-pair/recipeDetails?recipeId=${recipe.recipeId}">
				<div class="column">
				
				<img class="img-tile" src="img/${recipe.recipeId}.jpg" />
				
				<div class="recipe-name">${recipe.name}</div>
				<c:set var="starRound" value='${Math.round(recipe.averageRating)}'/>
				<img class="star" src="img/stars/${starRound}.png" />
				
				<div class="ingredient_number">${recipe.ingredients.size()} ingredients</div>
				</div>
			</a>
		</c:forEach>
	</div>
	
<%@include file="recipeFooter.jsp" %>
		